******************************
Documentação do projeto Ralabô
******************************

.. figure:: _static/imagens/Logos/LogoRalabov1.png
    :align: center
    :alt: Logo RALABÔ
    :figclass: align-center

Olá, seja bem vindo a documentação oficial do Ralabô. Aqui você encontrará todo o conteúdo para montagem e uso do ralador robô, assim como alguns exemplos de projetos. Se você quiser colaborar, ou viu algum erro, fique a vontade para entrar em contato com os administradores do projeto.

.. image:: _static/imagens/Diversos/RalaboFrenteVerso.png
   :width: 100%

A documentação está dividida em tópicos, onde cada tópico é um diretório com o conteúdo específico. Se tiver dificuldade para encontrar as informações, por favor deixe um feedback.

Se quiser entrar em contato com os administradores do projeto, envie um e-mail para: contato@hamiltonsena.net

O Ralabo foi criado com o intuito de ajudar professores que querem ensinar programação de forma concreta.

.. image:: _static/imagens/Circuitos/Ralabo_Cabeca_Relacao.bmp
   :width: 100%


Montando o seu Ralabô
=====================

.. toctree::
   :maxdepth: 2
   
   Lista de Materiais <MR_ListaMateriais.rst>
   Identificando os Componentes <MR_IdentificandoComponentes.rst>
   Montagem fisica <MR_Montagem.rst>
   Esquematico e Pinagem <MR_EsquematicoPinagem.rst>
   

Testando os Componetes do Ralabô
================================

.. toctree::
   :maxdepth: 2   
   
   BUZZER <TR_BUZZER.rst>
   BOTÃO <TR_BOTAO.rst>
   LDR <TR_LDR.rst>
   LCD <TR_LCD.rst>
   LED <TR_LED.rst>
   MOTOR <TR_MOTOR.rst>
   POTENCIOMETRO <TR_POTENCIOMETRO.rst>
   SERVO MOTOR <TR_SERVO.rst>
   ULTRASOM <TR_ULTRASOM.rst>
   

Dicas e Truques
===============

.. toctree::
   :maxdepth: 2   
   
   Utilizando o DB4K <DT_UtilizandoDB4K.rst>   
   

Exemplos de Projetos
====================

.. toctree::
   :maxdepth: 2
   
   Sistema semaforico <EX_Semaforo.rst>
